import { AppState, checkValidity, Dictionary, DictionaryEntry, makeDictionary, makeRow } from "../appdefinitions";
import * as Constants from "../constants";

// TODO needs better typing
export function reducer(state: AppState, action) {
    switch (action.type) {
        case Constants.DELETE_DICTIONARY: {
            const dictId = action.data;
            return state.filter((d) => d !== dictId);
        }
        case Constants.ADD_DICTIONARY: {
                return [...state, makeDictionary(action.data, [])];
        }
        case Constants.ADD_DICTIONARY_ROW: {
            const dictToAddRow = action.data.dict as Dictionary;
            return state.map((d) => {
                if (d === dictToAddRow) {
                    const entries = [...dictToAddRow.entries, makeRow("Original Value", "New Value")];
                    return {...dictToAddRow, validity: checkValidity(entries), entries};
                } else {
                    return d;
                }
            });
        }
        case Constants.EDIT_DICTIONARY_ROW: {
            const {newKey, newValue, index, dict} = action.data;
            const dictToChange = dict as Dictionary;

            return state.map((d) => {
                if (d === dictToChange) {
                    let entries = dictToChange.entries as DictionaryEntry[];
                    entries = entries.map((entry, idx) => {
                        if (idx === index) {
                            return makeRow(newKey, newValue);
                        } else {
                            return entry;
                        }
                    });
                    return {...dictToChange, entries, validity: checkValidity(entries)};
                } else {
                    return d;
                }
            });
        }
        case Constants.DELETE_DICTIONARY_ROW: {
            const id: string = action.data.id;
            const dict: Dictionary = action.data.dict;

            const entries = dict.entries.filter ((ent) => ent._id !== id);

            return state.map((d) => {
                if (d === dict) {
                    return {...dict, entries, validity: checkValidity(entries)};
                } else {
                    return d;
                }
            });
        }
        case Constants.CHANGE_DICTIONARY_TITLE: {
            const newTitle: string = action.data.newTitle;
            const dict: Dictionary = action.data.dict;

            return state.map((d) => {
                if (d === dict) {
                    return {...dict, name: newTitle};
                } else {
                    return d;
                }
            });
        }
        default:
            return state;
    }
}
