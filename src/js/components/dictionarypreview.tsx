import { green600, grey300, grey600, yellow600 } from "material-ui/styles/colors";
import CheckCircle from "material-ui/svg-icons/action/check-circle";
import Warning from "material-ui/svg-icons/alert/warning";
import SvgIcon from "material-ui/SvgIcon";

import { Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from "material-ui/Table";
import * as React from "react";
import { Link } from "react-router-dom";
import * as DictionaryModel from "../appdefinitions";

interface DictionaryPreviewProps {
    dict: DictionaryModel.Dictionary;
    maxRows: number;
}

const DictionaryStatusPreview = (props: {dict: DictionaryModel.Dictionary}) => {
    const color = props.dict.validity.isValid ? green600 : yellow600;
    return (
        <div style={{textAlign: "center"}}>
            {
                props.dict.validity.isValid ?
                [] :
                [<Warning color={yellow600}/>]
            }
        </div>
    );
};
export class DictionaryPreview extends React.Component<DictionaryPreviewProps, {}> {

    fillRows() {
        let rows: JSX.Element[];
        rows =
            this.props.dict.entries.slice(0, this.props.maxRows).map((entry, index) =>
                <TableRow key={index}>
                    <TableRowColumn>{entry.key}</TableRowColumn>
                    <TableRowColumn>{entry.value}</TableRowColumn>
                </TableRow>,
            );
        if (this.props.dict.entries.length > this.props.maxRows) {
            rows.push(
                <TableRow>
                    <TableRowColumn>
                        <span style={{color: grey300}}>More entries to show...</span>
                    </TableRowColumn>
                    <TableRowColumn/>
                </TableRow>);
        } else {
            let rowNum = this.props.dict.entries.length;
            if (rowNum === 0) {
                rows.push(
                    <TableRow>
                        <TableRowColumn>No entries yet...</TableRowColumn>
                        <TableRowColumn/>
                    </TableRow>);
                rowNum++;
            }
            for (let i = rowNum; i <= this.props.maxRows; i++) {
                rows.push(
                    <TableRow displayBorder={false}>
                        <TableRowColumn/><TableRowColumn/>
                    </TableRow>);
            }
        }
        return rows;
    }
    public render() {
        return (
            <div>
                <Table>
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                        enableSelectAll={false}
                    >
                        <TableRow>
                            <TableHeaderColumn>Original Value</TableHeaderColumn>
                            <TableHeaderColumn>Mapped Value</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {
                            this.fillRows()
                        }
                    </TableBody>
                </Table>
                <DictionaryStatusPreview dict={this.props.dict}/>
            </div>
        );
    }
}
