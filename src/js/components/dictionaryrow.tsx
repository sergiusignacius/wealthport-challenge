import RaisedButton from "material-ui/RaisedButton";
import { red100 } from "material-ui/styles/colors";
import { TableRow, TableRowColumn} from "material-ui/Table";
import TextField from "material-ui/TextField";
import * as React from "react";
import * as DictionaryModel from "../appdefinitions";

interface DictionaryRowProps {
    entry: DictionaryModel.DictionaryEntry;
    onRowSave: (key: string, value: string) => void;
    onRowDelete: (_id: string) => void;
    validity: DictionaryModel.DictionaryValidity;
}

interface DictionaryRowState {
    isEditing: boolean;
}

export class DictionaryRow extends React.Component<DictionaryRowProps, DictionaryRowState> {

    private keyInput: TextField;
    private valueInput: TextField;

    private buttonStyle = {
        marginBottom: "12px",
        marginRight: "12px",
        marginTop: "12px",
    };

    constructor() {
        super();
        this.state = {isEditing: false};
    }

    public render() {
        if (this.state.isEditing) {
            return (
                <TableRow>
                    <TableRowColumn>
                        <TextField
                            ref={(input) => this.keyInput = input}
                            hintText="Key"
                            defaultValue={this.props.entry.key}
                        />
                    </TableRowColumn>
                    <TableRowColumn>
                        <TextField
                            ref={(input) => this.valueInput = input}
                            hintText="Value"
                            defaultValue={this.props.entry.value}
                        />
                    </TableRowColumn>
                    <TableRowColumn>
                        <RaisedButton label="Cancel" style={this.buttonStyle} onClick={this.handleCancelClick}/>
                        <RaisedButton
                            label="Save"
                            primary={true}
                            style={this.buttonStyle}
                            onClick={() => this.handleSaveClick(this.keyInput.getValue(), this.valueInput.getValue())}
                        />
                    </TableRowColumn>
                </TableRow>
            );
        } else {
            const validity = this.props.validity;
            const entries = validity.offendingEntries || [];
            const index = entries.map((r) => r._id).indexOf(this.props.entry._id);
            const isOffendingRow = index >= 0;
            return (
                <TableRow style={{backgroundColor: isOffendingRow ? red100 : "inherit"}}>
                    <TableRowColumn>{this.props.entry.key}</TableRowColumn>
                    <TableRowColumn>{this.props.entry.value}</TableRowColumn>
                    <TableRowColumn>
                        <RaisedButton label="Edit" primary={true} style={this.buttonStyle} onClick={this.handleEditClick}/>
                        <RaisedButton
                            label="Delete"
                            secondary={true}
                            style={this.buttonStyle}
                            onClick={() => this.props.onRowDelete(this.props.entry._id)}
                        />
                    </TableRowColumn>
                </TableRow>
            );
        }
    }

    private componentDidUpdate() {
        if (this.state.isEditing) {
            this.keyInput.focus();
        }
    }

    private setIsEditing(isEditing: boolean) {
        this.setState({ isEditing });
    }

    private handleEditClick = () => {
        this.setIsEditing(true);
    }

    private handleCancelClick = () => {
        this.setIsEditing(false);
    }

    private handleSaveClick = (newKey: string, newValue: string) => {
        this.props.onRowSave(newKey, newValue);
        this.handleCancelClick();
    }
}
