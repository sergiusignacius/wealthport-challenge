import RaisedButton from "material-ui/RaisedButton";
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from "material-ui/Table";
import * as React from "react";
import * as DictionaryModel from "../appdefinitions";
import { DictionaryRow } from "./dictionaryrow";

interface DictionaryProps {
    dict: DictionaryModel.Dictionary;
    onRowSave: (newKey: string, newValue: string, index: number, dict: DictionaryModel.Dictionary) => void;
    onRowDelete: (id: string, dict: DictionaryModel.Dictionary) => void;
    onNewRow: (dict: DictionaryModel.Dictionary) => void;
}

export class Dictionary extends React.Component<DictionaryProps, {}> {
    public render() {
        const validity = this.props.dict.validity;
        return (
            <div>
                <Table>
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                        enableSelectAll={false}
                    >
                        <TableRow>
                            <TableHeaderColumn>Original Value</TableHeaderColumn>
                            <TableHeaderColumn>Mapped Value</TableHeaderColumn>
                            <TableHeaderColumn>Actions</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {
                            this.props.dict.entries.map((entry, index) =>
                                <DictionaryRow
                                    entry={entry}
                                    key={index}
                                    onRowSave={(k, v) => this.props.onRowSave(k, v, index, this.props.dict)}
                                    onRowDelete={(id) => this.props.onRowDelete(id, this.props.dict)}
                                    validity={validity}
                                />)
                        }
                    </TableBody>
                </Table>
                <RaisedButton label="Add Row" fullWidth={true} onClick={() => this.props.onNewRow(this.props.dict)}/>
            </div>
        );
    }
}
