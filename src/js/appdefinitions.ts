import { v4 as newUuid } from "node-uuid";
import { validationFunctions } from "./validations/validations";

export interface DictionaryEntry {
    _id: string;
    key: string;
    value: string;
}

export interface Dictionary {
    _id: string;
    entries: DictionaryEntry[];
    name: string;
    validity: DictionaryValidity;
}

export enum OffenseType {
    DuplicateEntries,
    DuplicateDomains,
    Cycle,
    Chain,
}

export interface DictionaryValidity {
    isValid: boolean;
    offendingEntries?: DictionaryEntry[];
    offenseType?: OffenseType;
}

export function makeRow(key: string, value: string): DictionaryEntry {
    return {
        _id: newUuid(),
        key,
        value,
    };
}

// priority-ordered validity functions
const validityFunctions: Array<(entries: DictionaryEntry[]) => DictionaryValidity> = [
    validationFunctions.hasCycleDetection,
    validationFunctions.hasChainDetection,
    validationFunctions.hasDuplicateDomainsDetection,
    validationFunctions.hasDuplicateEntriesDetection,
];

export function checkValidity(entries: DictionaryEntry[]) {
    let validity: DictionaryValidity = {
        isValid: true,
    };
    let i = 0;
    while (validity.isValid && i < validityFunctions.length) {
        validity = validityFunctions[i](entries);
        i++;
    }

    return validity;
}

export function makeDictionary(name: string, entries: DictionaryEntry[]): Dictionary {
    return {
        _id: newUuid(),
        name,
        entries,
        validity: checkValidity(entries),
    };
}

function entryRepresentation(entry: DictionaryEntry) {
    return `(${entry.key} -> ${entry.value})`;
}

export function getValidityErrorMessage(validity: DictionaryValidity) {
    const offendingEntries = validity.offendingEntries.map(entryRepresentation).join(", ");

    switch (validity.offenseType) {
        case OffenseType.DuplicateDomains:
            return `Duplicate domains detected in entries ${offendingEntries}`;
        case OffenseType.DuplicateEntries:
            return `Duplicate entries ${offendingEntries}`;
        case OffenseType.Cycle:
            return `Cycle detected between entries ${offendingEntries}`;
        case OffenseType.Chain:
            return `Chain detected between entries ${offendingEntries}`;
    }
}

export type AppState = Dictionary[];
