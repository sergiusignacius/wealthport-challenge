import { AppState, Dictionary } from "./appdefinitions";

export function loadState(): AppState {
    try {
        const serializedState = localStorage.getItem("state");
        if (serializedState === null) {
            return [];
        }
        return JSON.parse(serializedState);
    } catch (err) {
        console.error(err);
        return undefined;
    }
}

export function saveState(state: AppState) {
    try {
        localStorage.setItem("state", JSON.stringify(state));
    } catch (err) {
        // tslint:disable-next-line:no-console
        console.log(err);
    }
}
