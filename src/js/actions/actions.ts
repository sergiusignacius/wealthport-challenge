import { Dictionary } from "../appdefinitions";
import * as Constants from "../constants";

export function addDictionary(dispatch: any, name: string) {
    dispatch({ type: Constants.ADD_DICTIONARY, data: name });
}

export function deleteDictionary(dispatch: any, dict: Dictionary) {
    dispatch({ type: Constants.DELETE_DICTIONARY, data: dict});
}

export function addDictionaryRow(dispatch: any, dict: Dictionary) {
    dispatch({ type: Constants.ADD_DICTIONARY_ROW, data: { dict }});
}

export function deleteDictionaryRow(dispatch: any, id: string, dict: Dictionary) {
    dispatch({ type: Constants.DELETE_DICTIONARY_ROW, data: {id, dict}});
}

export function editDictionaryRow(dispatch: any, newKey: string, newValue: string, index: number, dict: Dictionary) {
    dispatch({ type: Constants.EDIT_DICTIONARY_ROW, data: {newKey, newValue, index, dict} });
}

export function changeDictionaryTitle(dispatch: any, newTitle: string, dict: Dictionary) {
    dispatch({ type: Constants.CHANGE_DICTIONARY_TITLE, data: {newTitle, dict}});
}
