import FloatingActionButton from "material-ui/FloatingActionButton";
import { GridList, GridTile } from "material-ui/GridList";
import IconButton from "material-ui/IconButton";
import DeleteForever from "material-ui/svg-icons/action/delete-forever";
import ContentAdd from "material-ui/svg-icons/content/add";
import Edit from "material-ui/svg-icons/editor/mode-edit";
import * as React from "react";
import { connect } from "react-redux";
import { Link, RouteComponentProps } from "react-router-dom";
import {
    addDictionary,
    deleteDictionary,
} from "../actions/actions";
import { AppState, Dictionary } from "../appdefinitions";
import { DictionaryPreview } from "../components/dictionarypreview";
import * as Constants from "../constants";

interface DictionaryOverviewEntryProps {
    dictionariesInRow: Dictionary[];
    onDeleteClick: (d: Dictionary) => void;
}

export interface DictionaryOverviewProps extends RouteComponentProps<{}> {
    dictionaries: Dictionary[];
    onDeleteClick: (dict: Dictionary) => void;
    onAddDictionaryClick: () => void;
}

const floating: React.CSSProperties = {
    bottom: 20,
    left: "auto",
    margin: 0,
    position: "fixed",
    right: 20,
    top: "auto",
    zIndex: 10,
};

const styles = {
    gridList: {
        height: 350,
        width: "100%",
    },
    root: {
        display: "flex",
    },
};

class DictionaryOverview extends React.Component<DictionaryOverviewProps, {}> {
    public render() {
        return (
            <div>
                <div style={styles.root}>
                    <GridList
                        cellHeight={300}
                        cols={3}
                        style={styles.gridList}
                    >
                    {
                        this.props.dictionaries.map((dict, index) => (
                            <GridTile
                                key={index}
                                title={dict.name}
                                actionIcon={
                                    <div>
                                        <Link to={"/dictionary/" + dict._id}>
                                            <IconButton>
                                                <Edit color="white" />
                                            </IconButton>
                                        </Link>
                                        <IconButton onClick={(e) => this.props.onDeleteClick(dict)}>
                                            <DeleteForever color="white" />
                                        </IconButton>
                                    </div>
                                }
                            >
                                <DictionaryPreview dict={dict} maxRows={2}/>
                            </GridTile>
                        ))
                    }
                    </GridList>
                </div>
                <FloatingActionButton style={floating} secondary={true} onClick={() => this.props.onAddDictionaryClick()}>
                    <ContentAdd />
                </FloatingActionButton>
            </div>
        );
    }
}

const mapStateToProps = (state: AppState, ownProps: DictionaryOverviewProps) => {
    return {
        dictionaries: state,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onAddDictionaryClick: () => addDictionary(dispatch, "New Dictionary"),
        onDeleteClick: (d: Dictionary) => deleteDictionary(dispatch, d),
    };
};

export let DictionaryOverviewPage = connect(mapStateToProps, mapDispatchToProps)(DictionaryOverview);
