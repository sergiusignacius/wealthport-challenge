import RaisedButton from "material-ui/RaisedButton";
import {green500, red500} from "material-ui/styles/colors";
import Subheader from "material-ui/Subheader";
import ErrorOutline from "material-ui/svg-icons/alert/error-outline";
import Check from "material-ui/svg-icons/navigation/check";
import SvgIcon from "material-ui/SvgIcon";
import TextField from "material-ui/TextField";
import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import {
    addDictionaryRow,
    changeDictionaryTitle,
    deleteDictionaryRow,
    editDictionaryRow,
} from "../actions/actions";
import { AppState, Dictionary, DictionaryValidity, getValidityErrorMessage } from "../appdefinitions";
import { Dictionary as DictionaryComponent } from "../components/dictionary";
import * as Constants from "../constants";

interface DictionaryEditProps extends RouteComponentProps<any> {
    dict: Dictionary;
    onRowSave: (newKey: string, newValue: string, index: number, dict: Dictionary) => void;
    onNewRow: (dict: Dictionary) => void;
    onRowDelete: (id: string, dict: Dictionary) => void;
    onTitleChanged: (newTitle: string, dict: Dictionary) => void;
}

interface DictionaryTitleProps {
    title: string;
    onTitleChanged: (newTitle: string) => void;
}

interface DictionaryTitleState { isEditingTitle: boolean; }

const headerStyles = {
  headline: {
    display: "inline",
    fontSize: 24,
    marginBottom: 12,
    marginRight: 12,
    paddingTop: 16,
  },
};

const DictionaryStatus = (props: {validity: DictionaryValidity}) => {
    if (props.validity.isValid) {
        return (
            <div style={{color: green500}}>
                This dictionary is consistent!
            </div>
        );
    } else {
        return (
            <div style={{color: red500}}>
                This dictionary is inconsistent! Problem: { getValidityErrorMessage(props.validity) }
            </div>
        );
    }
};

class DictionaryTitle extends React.Component<DictionaryTitleProps, DictionaryTitleState> {
    private titleInput: TextField;

    constructor() {
        super();
        this.state = { isEditingTitle: false };
    }

    public render() {
        if (this.state.isEditingTitle) {
            return (
                <div style={{marginTop: 12}}>
                    <TextField
                        ref={(input) => this.titleInput = input}
                        hintText="Title"
                        defaultValue={this.props.title}
                    />
                    <RaisedButton label="Cancel" style={{ marginLeft: 12, marginRight: 12}} onClick={this.handleCancel}/>
                    <RaisedButton label="Save" onClick={this.handleTitleInputBlur}/>
                </div>
            );
        } else {
            return (
                <div style={{marginTop: 12}}>
                    <h2 style={headerStyles.headline}>
                        {this.props.title}
                    </h2>
                    <RaisedButton label="Edit" onClick={this.handleTitleClick}/>
                </div>
            );
        }
    }

    private handleCancel = () => {
        this.setState({ isEditingTitle: false });
    }
    private handleTitleClick = () => {
        this.setState({ isEditingTitle: true });
    }

    private handleTitleInputBlur = () => {
        this.props.onTitleChanged(this.titleInput.getValue());
        this.setState({ isEditingTitle: false});
    }
}

class DictionaryDisplay extends React.Component<DictionaryEditProps, {}> {
    public render() {
        return (
            <div>
                <DictionaryTitle title={this.props.dict.name} onTitleChanged={(t) => this.props.onTitleChanged(t, this.props.dict)}/>
                <Subheader>
                    <DictionaryStatus validity={this.props.dict.validity}/>
                </Subheader>
                <DictionaryComponent
                    dict={this.props.dict}
                    onRowSave={this.props.onRowSave}
                    onNewRow={this.props.onNewRow}
                    onRowDelete={this.props.onRowDelete}/>
            </div>
        );
    }
}

const mapStateToProps = (state: AppState, ownProps: DictionaryEditProps) => {
    return {
        dict: state.filter((dic) => dic._id === ownProps.match.params.id)[0],
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onNewRow: (dict: Dictionary) => {
            addDictionaryRow(dispatch, dict);
        },
        onRowDelete: (id: string, dict: Dictionary) => {
            deleteDictionaryRow(dispatch, id, dict);
        },
        onRowSave: (newKey: string, newValue: string, index: number, dict: Dictionary) => {
            editDictionaryRow(dispatch, newKey, newValue, index, dict);
        },
        onTitleChanged: (newTitle: string, dict: Dictionary) => {
            changeDictionaryTitle(dispatch, newTitle, dict);
        },
    };
};

export let DictionaryEdit = connect(mapStateToProps, mapDispatchToProps)(DictionaryDisplay);
