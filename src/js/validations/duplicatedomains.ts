import { DictionaryEntry, DictionaryValidity, OffenseType } from "../appdefinitions";

// O(n) time, O(n) space where n is the number of dictionary entries
export function hasDuplicateDomains(entries: DictionaryEntry[]): DictionaryValidity {
    if (!entries.length) {
        return { isValid: true };
    }
    const set = {};
    const result: DictionaryValidity = { isValid: true };

    for (const entry of entries) {
        if (set.hasOwnProperty(entry.key) && set[entry.key].value !== entry.value) {
            result.isValid = false;
            result.offendingEntries = [set[entry.key], entry];
            result.offenseType = OffenseType.DuplicateDomains;
            break;
        } else {
            set[entry.key] = entry;
        }
    }
    return result;
}
