import { DictionaryEntry, DictionaryValidity , OffenseType} from "../appdefinitions";

function compareStrings(a: string, b: string) {
    if (a === b) {
        return 0;
    } else if (a < b) {
        return -1;
    } else {
        return 1;
    }
}

// O(n log n), O(n) space, where n is the number of dictionary entries
export function hasDuplicateEntries(dictEntries: DictionaryEntry[]): DictionaryValidity {
    if (!dictEntries.length) {
        return { isValid: true };
    }

    const entries = dictEntries.slice();
    entries.sort((a, b) => {
        const keyResult = compareStrings(a.key, b.key);
        if (keyResult === 0) {
            return compareStrings(a.value, b.value);
        } else {
            return keyResult;
        }
    });

    let currentElement = entries[0];
    let hasDuplicates = false;
    const offendingEntries: DictionaryEntry[] = [];
    for (let i = 1; i < entries.length; i++) {
        const entry = entries[i];
        if (currentElement.key === entry.key && currentElement.value === entry.value) {
            hasDuplicates = true;
            offendingEntries.push(currentElement);
            offendingEntries.push(entry);
            break;
        } else {
            currentElement = entry;
        }
    }
    if (hasDuplicates) {
        return {
            isValid: !hasDuplicates,
            offendingEntries,
            offenseType: OffenseType.DuplicateEntries,
        };
    } else {
        return { isValid: true };
    }
}
