import { DictionaryEntry, DictionaryValidity, OffenseType } from "../appdefinitions";

interface GraphAdjacency {
    value: string;
    entry: DictionaryEntry;
}

interface CycleResult {
    hasCycle: boolean;
    path?: StringDictionary<PathStep>;
    cycleStart?: PathStep;
}

interface PathStep {
    vertex: string;
    entry: DictionaryEntry;
}

// tslint:disable-next-line:interface-over-type-literal
type StringDictionary<V> = { [id: string]: V; };

/*
 *  detect the first cycle in dictionary entries using DFS.
 *  Runs in O (|V| + |E|) time where V is the number
 *  of unique domain values and E is the number of dictionary entries
*/
function detectCycle(graph: StringDictionary<GraphAdjacency[]>): CycleResult {
    const colors: StringDictionary<string> = {};
    const predecessor: StringDictionary<PathStep> = {};

    for (const n in graph) {
        if (graph.hasOwnProperty(n)) {
            colors[n] = "white";
            predecessor[n] = undefined;
        }
    }

    for (const n in graph) {
        if (colors[n] === "white") {
            const cycleResult = dfsVisit(graph, n, undefined, colors, predecessor);
            if (cycleResult.hasCycle) {
                return cycleResult;
            }
        }
    }

    return { hasCycle: false };
}

function dfsVisit(
    graph: StringDictionary<GraphAdjacency[]>,
    vertex: string,
    currentEntry: DictionaryEntry,
    colors: StringDictionary<string>,
    predecessor: StringDictionary<PathStep>): CycleResult {

    colors[vertex] = "gray";

    for (const u of graph[vertex]) {
        const successor = u.value;
        const foundByDifferentAncestor = !predecessor[successor] || predecessor[successor].vertex !== vertex;

        if (colors[successor] === "gray" && foundByDifferentAncestor) {
            return { hasCycle: true, path: predecessor, cycleStart: { vertex, entry: u.entry} };
        }

        if (colors[successor] === "white") {
            predecessor[successor] = { vertex, entry: u.entry };
            const result = dfsVisit(graph, successor, u.entry, colors, predecessor);
            if (result.hasCycle) {
                return result;
            }
        }
    }

    colors[vertex] = "black";
    return { hasCycle: false };
}

function reconstructPath(adjList: StringDictionary<GraphAdjacency[]>, result: CycleResult): DictionaryEntry[] {
    const path = result.path;
    const cycleStart = result.cycleStart;

    let currentStep = cycleStart;
    const offendingEntries = [];
    while (path[currentStep.vertex] !== undefined) {
        offendingEntries.unshift(path[currentStep.vertex].entry);
        currentStep = path[currentStep.vertex];
    }

    offendingEntries.push(cycleStart.entry);

    return offendingEntries;
}

function initializeAdjacencyList(dictEntries: DictionaryEntry[]) {
    const adjList: StringDictionary<GraphAdjacency[]> = {};

    for (const entry of dictEntries) {
        if (!adjList[entry.key]) {
            adjList[entry.key] = [];
        }
        if (!adjList[entry.value]) {
            adjList[entry.value] = [];
        }
        adjList[entry.key].push({value: entry.value, entry});
    }

    return adjList;
}

export function hasCycle(dictEntries: DictionaryEntry[]): DictionaryValidity {
    if (!dictEntries.length) {
        return { isValid: true };
    }
    const adjList = initializeAdjacencyList(dictEntries);
    const result = detectCycle(adjList);
    if (result.hasCycle) {
        return { isValid: false, offendingEntries: reconstructPath(adjList, result), offenseType: OffenseType.Cycle };
    } else {
        return { isValid: true };
    }
}
