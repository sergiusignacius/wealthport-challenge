import { DictionaryEntry } from "../appdefinitions";
import { hasChain } from "./chaindetection";
import { hasCycle } from "./cycledetection";
import { hasDuplicateDomains } from "./duplicatedomains";
import { hasDuplicateEntries } from "./duplicaterows";

export let validationFunctions = {
    hasChainDetection: hasChain,
    hasCycleDetection: hasCycle,
    hasDuplicateDomainsDetection: hasDuplicateDomains,
    hasDuplicateEntriesDetection: hasDuplicateEntries,
};
