import { DictionaryEntry, DictionaryValidity, OffenseType } from "../appdefinitions";

export function hasChain(dictEntries: DictionaryEntry[]): DictionaryValidity {
    const domain: { [id: string]: DictionaryEntry} = {};
    const range: { [id: string]: DictionaryEntry} = {};

    for (const entry of dictEntries) {
        domain[entry.key] = entry;
        range[entry.value] = entry;
    }

    let hasChain = false;
    let offendingEntries: DictionaryEntry[];

    for (const key in domain) {
        if (domain.hasOwnProperty(key)) {
            if (range.hasOwnProperty(key)) {
                if (domain[key]._id !== range[key]._id) {
                    hasChain = true;
                    offendingEntries = [domain[key], range[key]];
                    break;
                }
            }
        }
    }

    if (hasChain) {
        return {
            isValid: false,
            offendingEntries,
            offenseType: OffenseType.Chain,
        };
    } else {
        return { isValid: true };
    }
}
