import { throttle } from "lodash";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import { createStore, Store } from "redux";
import { AppState } from "./appdefinitions";
import { loadState, saveState } from "./localstorage";

import AppBar from "material-ui/AppBar";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { reducer } from "./reducer/reducer";
import { DictionaryEdit } from "./views/dictionaryedit";
import { DictionaryOverviewPage } from "./views/dictionaryoverview";

class App extends React.Component<{}, {}> {
    public render() {
        return (
            <div className="container"  style={{fontFamily: "'Roboto', sans-serif"}}>
                <Link to="/" style={{textDecoration: "none"}}>
                    <AppBar
                        title="Wealthport Dictionary App"
                        showMenuIconButton={false}
                    />
                </Link>
                <div className="container">
                    { this.props.children }
                </div>
            </div>
        );
    }
}

const store: Store<AppState> = createStore(reducer, loadState());

store.subscribe(throttle(() => {
    saveState(store.getState());
}, 1000));

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider>
            <Router>
                <App>
                    <div>
                        <Route exact path="/" component={DictionaryOverviewPage}/>
                        <Route path="/dictionary/:id" component={DictionaryEdit} />
                    </div>
                </App>
            </Router>
        </MuiThemeProvider>
    </Provider>,
    document.getElementById("app-container"));
