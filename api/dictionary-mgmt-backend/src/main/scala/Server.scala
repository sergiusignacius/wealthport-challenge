import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer

import scala.concurrent.Await
import scala.concurrent.duration.Duration

import scala.io.StdIn

object WebServer {

  val publicFolder = "../../public"

  def index = () => {
    getFromFile(publicFolder + "/index.html")
  }

  def public = () => {
    getFromDirectory(publicFolder)
  }
  def main(args: Array[String]) {

    implicit val system = ActorSystem("my-system")
    implicit val materializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher

    val route =
      get {
        pathEndOrSingleSlash {
          index()
        } ~
        pathPrefix("dictionary") {
          index()
        } ~
        public()
      }

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    Await.result(bindingFuture, Duration.Inf)
  }
}