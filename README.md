# README #

### What is this repository for? ###
Home of the Wealthport Dictionary Management App :)

### How do I get set up? ###

* Clone this repo
* On the repo root, run `npm install && npm run buildandrun`
* Access `http://localhost:5000`
* Use and abuse it :)
* If you want to run tests, run `npm run test`

### How to use? ###
On the homepage - the Overview page -, there's a floating action button that allows you to create a new dictionary.

Afterwards each dictionary gets its place in the overview grid, where you can delete it or edit it.

In the edition page, you get to add and edit dictionary entries at your will, and all the validations will occur at each step.

### Main technologies ###

* React
* React Router
* Redux
* Webpack
* Material-UI

### Improvements ###

Some UX hiccups and code could be improved here and there, but time is short!

* Make the app look more like Wealthport real estate (like the website), but I'm terrible at design at the moment :)
* Dictionary overview could make better use of the space of each grid tile and perhaps have some text accompanying the warning icon whenever something's wrong
* Dictionary name edition is still a bit weird
* We could improve the way the inconsistency is displayed: for now, we just display the offending entries like (A -> B), ... and highlight the corresponding rows in the edition page
* Transitions are really rough (or non-existent) :)
* Floating action button, as well as the edit and delete icons in the overview page could use some tooltips
* Code reuse could be stronger between dictionary and dictionarypreview components
* Need to cleanup some inline styles
* Some integration tests (e.g., what DOM is rendered)
* Need to add server-side route to allow refreshing in the Edit screen