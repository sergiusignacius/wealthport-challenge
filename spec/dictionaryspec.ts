import { createStore, Dispatch, Store } from "redux";
import {
    addDictionary,
    addDictionaryRow,
    changeDictionaryTitle,
    deleteDictionary,
    deleteDictionaryRow,
    editDictionaryRow,
} from "../src/js/actions/actions";
import {
    AppState,
    Dictionary,
    DictionaryEntry,
    makeDictionary,
    makeRow,
    OffenseType,
} from "../src/js/appdefinitions";
import * as Constants from "../src/js/constants";
import { reducer } from "../src/js/reducer/reducer";

describe("", () => {
    let store: Store<AppState>;
    let dispatch: Dispatch<AppState>;
    beforeEach(() => {
        store = createStore(reducer, []);
        dispatch = store.dispatch;
    });

    function checkValidity(dict: Dictionary, isValid: boolean, offenseLength?: number, offenseType?: OffenseType) {
        if (isValid) {
            expect(dict.validity.isValid).toBeTruthy("This dictionary should be valid");
            expect(dict.validity.offenseType).toBeFalsy("Since the dictionary is valid, there should be no offense");
            expect(dict.validity.offendingEntries).toBeFalsy("Since the dictionary is valid, there should be no offense entries");
        } else {
            expect(dict.validity.isValid).toBeFalsy("This dictionary should be valid");
            expect(dict.validity.offenseType).toBe(offenseType, `Expected to find offenseType ${offenseType.toString()}`);
            expect(dict.validity.offendingEntries.length).toBe(offenseLength, `Expected to find ${offenseLength} offending entries`);
        }
    }

    describe("Dictionary store behavior", () => {
        it("should create empty dictionaries", () => {
            expect(store.getState().length).toBe(0);
            addDictionary(dispatch, "New");
            expect(store.getState().length).toBe(1);
            expect(store.getState()[0].name).toBe("New");
        });

        it("should create dictionaries rows", () => {
            expect(store.getState().length).toBe(0);
            addDictionary(dispatch, "New");

            let dict = store.getState()[0];
            expect(dict.entries.length).toBe(0, "No rows should exist yet");
            addDictionaryRow(dispatch, dict);

            dict = store.getState()[0];
            expect(dict.entries.length).toBe(1, "Expected one new row after firing row creation action");
        });

        it("should delete dictionaries", () => {
            expect(store.getState().length).toBe(0, "No dictionaries should exist yet");
            addDictionary(dispatch, "New");
            expect(store.getState().length).toBe(1, "Expected one new dictionary");
            deleteDictionary(dispatch, store.getState()[0]);
            expect(store.getState().length).toBe(0, "No dictionaries should exist after deleting the last one");
        });

        it("should delete dictionary rows", () => {
            expect(store.getState().length).toBe(0, "No dictionaries should exist yet");
            addDictionary(dispatch, "New");

            let dict = store.getState()[0];
            expect(dict.entries.length).toBe(0, "No rows should exist yet");
            addDictionaryRow(dispatch, dict);

            dict = store.getState()[0];
            expect(dict.entries.length).toBe(1, "Expected a new row");
            deleteDictionaryRow(dispatch, dict.entries[0]._id, dict);

            dict = store.getState()[0];
            expect(dict.entries.length).toBe(0, "Row should have disappeared");
        });

        it("should change dictionary title", () => {
            expect(store.getState().length).toBe(0, "No dictionaries should exist yet");
            addDictionary(dispatch, "New");

            expect(store.getState()[0].name).toBe("New");

            let dict = store.getState()[0];
            changeDictionaryTitle(dispatch, "Title", dict);
            dict = store.getState()[0];
            expect(store.getState()[0].name).toBe("Title");
        });

        it("should change dictionary rows", () => {
            expect(store.getState().length).toBe(0);
            addDictionary(dispatch, "New");

            let dict = store.getState()[0];
            expect(dict.entries.length).toBe(0, "No rows should exist yet");
            addDictionaryRow(dispatch, dict);

            dict = store.getState()[0];
            let entry = dict.entries[0];

            expect(entry.key).toBe("Original Value", "Expected default key for this row");
            expect(entry.value).toBe("New Value", "Expected default value for this row");

            const newKey = "MysticSilver";
            const newValue = "Silver";
            editDictionaryRow(dispatch, newKey, newValue, 0, dict);

            dict = store.getState()[0];
            entry = dict.entries[0];

            expect(entry.key).toBe("MysticSilver", "Expected key to change");
            expect(entry.value).toBe("Silver", "Expected value to change");
        });
    });

    describe("Dictionary validation functions", () => {
        it("should flag a dictionary as valid if no offenses occur", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("c", "d"));

            const dict = makeDictionary("testDictionary", entries);
            checkValidity(dict, true);
        });

        it("should detect self cycles", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "a"));

            const dict = makeDictionary("testDictionary", entries);

            const expectedCycleLength = 1;
            checkValidity(dict, false, expectedCycleLength, OffenseType.Cycle);
        });

        it("should detect simple cycles", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("b", "a"));

            const dict = makeDictionary("testDictionary", entries);

            const expectedCycleLength = 2;
            checkValidity(dict, false, expectedCycleLength, OffenseType.Cycle);
        });

        it("should detect long cycles", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("b", "c"));
            entries.push(makeRow("c", "d"));
            entries.push(makeRow("d", "a"));

            const dict = makeDictionary("testDictionary", entries);

            const expectedCycleLength = 4;
            checkValidity(dict, false, expectedCycleLength, OffenseType.Cycle);
        });

        it("should detect the first two duplicate rows, even if there are more duplicates", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("a", "b"));

            const dict = makeDictionary("testDictionary", entries);

            checkValidity(dict, false, 2, OffenseType.DuplicateEntries);
        });

        it("should detect the first two duplicate rows", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("d", "e"));
            entries.push(makeRow("a", "b"));

            const dict = makeDictionary("testDictionary", entries);

            checkValidity(dict, false, 2, OffenseType.DuplicateEntries);
        });

        it("should detect the first two entries with duplicate domains", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("a", "e"));

            const dict = makeDictionary("testDictionary", entries);

            checkValidity(dict, false, 2, OffenseType.DuplicateDomains);
        });

        it("should detect simple chains", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("b", "c"));

            const dict = makeDictionary("testDictionary", entries);

            checkValidity(dict, false, 2, OffenseType.Chain);
        });
    });

    function addEntries(entries: DictionaryEntry[], dict: Dictionary) {
        for (const entry of entries) {
            addDictionaryRow(dispatch, dict);
            const newDict = store.getState()[0];
            const lastIndex = newDict.entries.length - 1;
            editDictionaryRow(dispatch, entry.key, entry.value, lastIndex, newDict);
        }
    }

    describe("Validation behavior with changes over time", () => {
        it("should update validations whenever something changes", () => {
            const entries: DictionaryEntry[] = [];
            entries.push(makeRow("a", "b"));
            entries.push(makeRow("b", "c"));
            entries.push(makeRow("c", "d"));
            entries.push(makeRow("d", "a"));

            addDictionary(dispatch, "New");
            let dict = store.getState()[0];
            addEntries(entries, dict);

            editDictionaryRow(dispatch, "d", "e", 3, dict);

            dict = store.getState()[0];
            expect(dict.validity.offenseType).not.toBe(OffenseType.Cycle, "A cycle should have disappeared");
            expect(dict.validity.offenseType).not.toBe(OffenseType.Chain, "A chain should have appeared");
        });
    });

});
